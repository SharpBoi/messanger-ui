const { webpackDevConfig } = require('./webpack-dev.config');
const { webpackProdConfig } = require('./webpack-prod.config');

module.exports = env => {
  const isDev = env.MODE === 'development';
  const isProd = env.MODE === 'production';

  if (isDev) return webpackDevConfig;
  if (isProd) return webpackProdConfig;
}
