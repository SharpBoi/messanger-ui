const { webpackBaseConfig } = require('./webpack-base.config');
const { createTsxConfig } = require('./pipeline/configs/tsx.config');

module.exports.webpackDevConfig = {
  ...webpackBaseConfig,
  module: {
    rules: [...webpackBaseConfig.module.rules, createTsxConfig('dev')]
  },

  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  }
};
