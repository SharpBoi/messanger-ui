const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const { cssModulesConfig } = require('./pipeline/configs/css-modules.config');
const { cssConfig } = require('./pipeline/configs/css.config');
const { fileLoaderConfig } = require('./pipeline/configs/file-loader.config');
const { urlLoaderConfig } = require('./pipeline/configs/url-loader.config');
const { scssConfig } = require('./pipeline/configs/scss.config');

module.exports.webpackBaseConfig = {
  entry: ['./src/index.tsx'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]-[hash].js'
  },
  module: {
    rules: [cssModulesConfig, cssConfig, scssConfig, fileLoaderConfig, urlLoaderConfig]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html',
      title: 'Messanger ui'
    })
  ]
};
