const { webpackBaseConfig } = require('./webpack-base.config');
const TerserPlugin = require('terser-webpack-plugin');
const { createTsxConfig } = require('./pipeline/configs/tsx.config');

module.exports.webpackProdConfig = {
  ...webpackBaseConfig,
  module: {
    rules: [...webpackBaseConfig.module.rules, createTsxConfig('prod')]
  },

  mode: 'production',
  devtool: 'none',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false
      })
    ]
  }
};
