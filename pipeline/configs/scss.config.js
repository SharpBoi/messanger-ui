module.exports.scssConfig = {
  test: /\.s[ac]ss$/i,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        modules: {
          localIdentName: '[local]--[hash:base64:5]' //'[name]__[local]--[hash:base64:5]'
        }
      }
    },
    'sass-loader'
  ]
};
