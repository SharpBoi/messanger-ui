// file loader used for transpile static resources from statics
module.exports.fileLoaderConfig = {
  test: /\/(assets)\/(static)\/([^\s]+)\.(png|jpeg|jpg|svg)$/i,
  use: [
    {
      loader: 'file-loader'
    }
  ]
};
