import React from 'react';
import { MessageView } from '../../components/message-view/MessageView';
import { IMessage } from '../../interfaces/message.interface';
import style from './module.scss';

type Props = {
  temp_messages: IMessage[];
};
export function MessagesView({ temp_messages }: Props): JSX.Element {
  return (
    <div className={style.messages_view}>
      <div className={style.contents}>
        {temp_messages.map((msg, index) => (
          <MessageView key={index} content={msg.content} sender={msg.sender} />
        ))}
      </div>
    </div>
  );
}
