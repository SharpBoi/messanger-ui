import React, { useCallback, useState } from 'react';
import { ChatViewFooter } from '../../components/chat-view-footer/ChatViewFooter';
import { ChatViewHeader } from '../../components/chat-view-header/ChatViewHeader';
import { IMessage } from '../../interfaces/message.interface';
import { MessagesView } from '../messages-view/MessagesView';
import style from './module.scss';

export function ChatView(): JSX.Element {
  const [messages, setMessages] = useState<IMessage[]>([]);

  const handleMessageSubmission = useCallback(
    (content: string) => {
      setMessages(value => {
        const message: IMessage = {
          content,
          sender: value.length % 2 === 0 ? 'self' : 'other',
          date: Date.now()
        };

        return [...value, message];
      });
    },
    [messages]
  );

  return (
    <div className={style.chat_view}>
      <ChatViewHeader opponentName='Mike Kekson' />
      <MessagesView temp_messages={messages} />
      <ChatViewFooter onSubmitMessage={handleMessageSubmission} />
    </div>
  );
}
