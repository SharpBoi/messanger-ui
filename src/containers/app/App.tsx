import React from 'react';
import { ChatView } from '../chat-view/ChatView';
import style from './module.scss';

export function App(): JSX.Element {


  return (
    <div className={style.app}>
      <ChatView />
    </div>
  );
}
