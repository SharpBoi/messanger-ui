import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './containers/app/App';
import style from './index.scss';

const appRoot = document.querySelector('#app-root');
appRoot.classList.add(style.app_root);

ReactDOM.render(
  <App/>,
  appRoot
);
