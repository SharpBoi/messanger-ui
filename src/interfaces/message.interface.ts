import { SenderSide } from '../types/sender-side.type';

export interface IMessage {
  content: string;
  sender: SenderSide;
  date: number;
}
