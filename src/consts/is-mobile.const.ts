import { mobileDetect } from './mobile-detect.const';

export const isMobile = mobileDetect.mobile() !== null;
