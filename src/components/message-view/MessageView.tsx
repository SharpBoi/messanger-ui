import React from 'react';
import { SenderSide } from '../../types/sender-side.type';
import style from './module.scss';

interface Props {
  content: string;
  sender: SenderSide;
}
export function MessageView({ content, sender }: Props): JSX.Element {
  const isOtherSender = sender === 'other';

  return (
    <div className={`${style.message_view} ${isOtherSender && style.left_sided}`}>
      <div className={`${style.outline} ${isOtherSender && style.opponent_color}`}>
        <span className={style.message}>{content}</span>
        <span className={style.meta}>20:33</span>
      </div>
    </div>
  );
}
