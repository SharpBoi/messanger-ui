import React, { useCallback, useRef } from 'react';
import { isMobile } from '../../consts/is-mobile.const';
import { ENTER_KEY } from '../../consts/keys.const';
import style from './module.scss';

interface Props {
  onSubmitMessage?: (content: string) => void;
}
export function ChatViewFooter({ onSubmitMessage }: Props): JSX.Element {
  const inputRef = useRef<HTMLTextAreaElement>();

  const submitMessage = useCallback(() => {
    const { current } = inputRef;
    const { value } = current;

    if (value.length === 0) {
      return;
    }

    onSubmitMessage?.(value);
    setTimeout(() => {
      current.value = '';
    }, 1);
  }, [inputRef]);

  const handleInputEnter = useCallback((e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    const { keyCode, shiftKey } = e;
    const isEnter = keyCode === ENTER_KEY;
    const isSendEnter = isEnter && !shiftKey;

    if (isSendEnter && !isMobile) {
      e.preventDefault();
      submitMessage();
    }
  }, [submitMessage]);

  const handleSendButton = useCallback(() => {
    submitMessage();
  }, [submitMessage]);

  const handleClick = useCallback(() => {
    inputRef.current.focus();
  }, [inputRef]);

  return (
    <div className={style.chat_view_footer} onClick={handleClick}>
      <textarea
        className={style.message_input}
        placeholder='Type message...'
        onKeyDown={handleInputEnter}
        ref={inputRef}
      />
      <button className={style.send_button} onClick={handleSendButton}>
        {'Send'}
      </button>
    </div>
  );
}
