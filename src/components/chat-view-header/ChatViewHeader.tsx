import React from 'react';
import style from './module.scss';

interface Props {
  opponentName: string;
}
export function ChatViewHeader({ opponentName }: Props): JSX.Element {
  return (
    <div className={style.chat_view_header}>
      <span>{opponentName}</span>
    </div>
  );
}
